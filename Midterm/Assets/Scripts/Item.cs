﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Item : MonoBehaviour, IComparable
{
    private Guid _id;
    public Guid Id { get { return _id; } }
    private string _name;
    private double _weight;
    private InventorySlotId _slotId;
    private bool _isNatural;

	public virtual string Name { get { return _name; }}
	public virtual double Weight { get {return _weight; } }
	public virtual InventorySlotId SlotId { get {return _slotId;} }
	public virtual bool IsNatural { get {return _isNatural; } }

    public int CompareTo(object obj)
    {
        Item other = ((Item)obj);
        return this.Id.CompareTo(other.Id);
    }

    public override bool Equals(object obj)
    {
        if (obj == null) { return false; }
        if (this.GetType() != obj.GetType()) { return false; }

        var other = (Item)obj;
        return this.Id == other.Id;
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}
