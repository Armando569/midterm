﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {
    [SerializeField]
    protected StoredItems _bag;
    [SerializeField]
    protected EquippedItems _equipped;


    [SerializeField]
    protected string _name;
    [SerializeField]
	protected int _currentHealth;
    [SerializeField]
    protected bool _dead;

    public StoredItems Bag{get{return _bag;}}
    public EquippedItems Equipped{get{return _equipped; }}
    public string Name {get{return _name; }}
	public int CurrentHealth{get{return _currentHealth; }}
	public bool IsDead{get {return _dead; }}

    //public double CalcTotalWeight()
    //{

    //}
    public int CalcTotalAttackValue()
    {
        return _equipped.CalcTotalAttackValue();
    }
     public int CalcTotalDefenseValue()
    {
        return _equipped.CalcTotalDefenseValue();
    }
    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        if (_currentHealth <= 0)
            _dead = true;
        else
            _dead = false;
    }

    //public void Pickup(Item item)
    //{

    //}
    //public void UnequipAll()
    //{

    //}
}
