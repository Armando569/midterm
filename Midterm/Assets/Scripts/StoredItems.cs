﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoredItems : MonoBehaviour {

	private Item[] _items;

	private int _count;

	public int Count {
		get{return _count; }
    }
    public int Capacity
    {
        get
        {
            return _items.Length;
        }
    }

    public StoredItems(int size)
	{
        _items = new Item[size];
        _count = 0;

        //for (int i = 0; i < _items.Length; ++i) { }
	}

    public Item GetItem(int index)
    {
        return _items[index];
    }

    public Item SetItem(int index, Item item)
    {
        return _items[index] = item;      
    }

    public void AddItem(Item item)
    {
           if (_items[_count] == null)
        {
          _items[_count] = item;
            _count++;
        }
    }
    public void RemoveItem(Item item)
    {
        for (int i = 0;i<_items.Length;i++)
        {
            if(_items[i] == item)
            {
                _items[i] = null;
                return;
            }
        }
    }

    //public double CalcTotalWeight()
    //{

    //}
}
