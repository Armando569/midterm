﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InventorySlotId 
{	
		UNEQUIPPABLE, HELMET, CHESTPIECE, GRIEVES, VAMBRACES, WEAPON, POTION1, POTION2
}
