﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IArmor 
{
	int DefenseValue {
		get;
	}

	bool IsNatural {
		get;
	}
}
