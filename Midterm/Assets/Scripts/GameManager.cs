﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    [SerializeField]
    private Character _player;
    [SerializeField]
    private Character _enemy;
    [SerializeField]
    private int _depth;
    [SerializeField]
    private bool _gameover;

    [SerializeField]
    private GameObject _playerPrefab;
    [SerializeField]
    private GameObject _enemyPrefab;
    [SerializeField]
    private GameObject _combatScreen;
    [SerializeField]
    private GameObject _inventoryScreen;
    [SerializeField]
    private GameObject _GameoverScreen;
    [SerializeField]
    private GameObject _StartScreen;





    // Use this for initialization
    void Start () {
        _StartScreen.SetActive(true);
        _combatScreen.SetActive(true);
        _GameoverScreen.SetActive(false);
        _inventoryScreen.SetActive(false);
        Debug.Log("Game started");
        _gameover = false;
        //_player = new Character();
        //_enemy = new Character();
     
    }
	
	// Update is called once per frame
	//void Update () {
	//}

    public void Attack()
    {
        
        Debug.Log("Attack");
        _enemy.TakeDamage(_player.CalcTotalAttackValue() - _enemy.CalcTotalDefenseValue());
        _player.TakeDamage(_enemy.CalcTotalAttackValue() - _player.CalcTotalDefenseValue());
        Debug.Log(_enemy.CurrentHealth);
        Debug.Log(_player.CurrentHealth);
        if (_enemy.IsDead == true)
        {
            Debug.Log("Dead");
            Destroy(_enemy.gameObject);
            GameObject newEnemy = GameObject.Instantiate(_enemyPrefab, _combatScreen.transform);
            //Component comp= newEnemy.GetComponent(typeof(Character));
            //_enemy = (Character)comp;
            
            _enemy = newEnemy.GetComponent<Character>();

            _inventoryScreen.SetActive(true);
        }
        if (_player.IsDead == true)
        {
            _gameover = true;
           _GameoverScreen.SetActive(true);
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}



