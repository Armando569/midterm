﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BronzeSword : Item, IWeapon
{
    private System.Guid _id;
   
    public BronzeSword()
    {
        _id = System.Guid.NewGuid();
    }

	public override string Name {
		get {
			return "Bronze Sword";
		}
	}

	public override double Weight {
		get {
			return 2;
		}
	}
	public override InventorySlotId SlotId {
		get {
			return InventorySlotId.WEAPON;
		}
	}
	public override bool IsNatural {
		get {
			return false;
		}
	}

 public int AttackValue
	{
		get{return Random.Range (4, 10); }
	}

    

}
