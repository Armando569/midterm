﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquippedItems : MonoBehaviour {

	private Item[] _slots;

    
	public EquippedItems()
	{
        _slots = new Item[7];      
	}

    public Item GetItem(InventorySlotId slot)
    {
        return _slots[(int) slot];
    }
    public Item Equip(InventorySlotId slot, Item item)
    {
        //FIX   should return what was inside the slot 
        return _slots[(int)slot] = item;
    }
    public Item Unequip(InventorySlotId slot)
    {
        //FIX    should return what was inside the slot 
        return _slots[(int)slot] = null;
    }

    //public double CalcTotalWeight()
    //{

    //}
    BronzeSword sword;
    public int CalcTotalAttackValue()
    {

        IWeapon weapon = (IWeapon)_slots[(int)InventorySlotId.WEAPON];
        sword = (BronzeSword)weapon;
        if (weapon == null)
        {
            return 1;
        }
        else
        {
            return weapon.AttackValue;
        }
    }
    public int CalcTotalDefenseValue()
    {
        int totalDefenseValue = 0;

       IArmor helmet = (IArmor)_slots[(int)InventorySlotId.HELMET];
        if (helmet == null)
        {
            totalDefenseValue += 0;
        }
        else
        {
            totalDefenseValue += helmet.DefenseValue;
        }
        IArmor chestpiece = (IArmor)_slots[(int)InventorySlotId.CHESTPIECE];
        if (chestpiece == null)
        {
            totalDefenseValue += 0;
        }
        else
        {
            totalDefenseValue += chestpiece.DefenseValue;
        }
        IArmor grieves = (IArmor)_slots[(int)InventorySlotId.GRIEVES];
        if (grieves == null)
        {
            totalDefenseValue += 0;
        }
        else
        {
            totalDefenseValue += grieves.DefenseValue;
        }
        IArmor vambraces = (IArmor)_slots[(int)InventorySlotId.VAMBRACES];
        if (helmet == null)
        {
            totalDefenseValue += 0;
        }
        else
        {
            totalDefenseValue += vambraces.DefenseValue;
        }
        return totalDefenseValue;
    }


}
