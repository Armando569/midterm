﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class Gambeson : Item, IArmor
{
	
	private System.Guid _id;
	public System.Guid Id{ get {return _id;}}

	public override string Name {
		get {
			return "Gambeson";
		}
	}

	public override double Weight {
		get {
			return 1;
		}
	}
	public override InventorySlotId SlotId {
		get {
			return InventorySlotId.CHESTPIECE;
		}
	}
	public override bool IsNatural {
		get {
			return false;
		}
	}

	int IArmor.DefenseValue
	{
		get{return Random.Range (0, 3); }
	}

	/*bool IArmor.IsNatural
	{
		get{return false; }
	}*/



}
