﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : MonoBehaviour
{
    public float startingPosX;
    public float startingPosY;
    public int slotCountPerPage;
    public int slotCountLength;
    public GameObject itemSlot;

    //private int _xPos;
    //private int _yPos;
    //private GameObject _itemSlot;
    //private int _itemSlotCnt;


    // Use this for initialization
    void Start()
    {
        CreateBagSlotsInWindow();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreateBagSlotsInWindow()
    {
        float _xPos = startingPosX;
        float _yPos = startingPosY;
        int _itemSlotCnt = 0;
        for (int i = 0; i < slotCountPerPage; i++)
        {
            GameObject _itemSlot = Instantiate(itemSlot, this.transform);
            //_itemSlot.transform.SetParent(gameObject.transform
            var rect = _itemSlot.GetComponent<RectTransform>();
            rect.localPosition = new Vector3(_xPos, _yPos, 0);
            _xPos += rect.rect.width;
            _itemSlotCnt++;
            if (_itemSlotCnt % slotCountLength == 0)
            {
                _itemSlotCnt = 0;
                _yPos -= rect.rect.width;
                _xPos = startingPosX;
            }
        }
    }



}


